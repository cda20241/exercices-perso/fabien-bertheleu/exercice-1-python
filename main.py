

def comptage_voyelles(x):
    phrase = x.lower()
    compteur = 0
    voyelles = "aoeyiu"
    liste_voyelles=["a", "e", "i", "o", "u", "y"]

    for lettre in phrase:
        for voyelle in voyelles :
            if lettre == voyelle:
                compteur = compteur + 1
    return compteur

def occurence_voyelle(x):
    occurence = {}
    phrase = x.lower()
    voyelles = "aoeyiu"
    liste_voyelles=["a", "e", "i", "o", "u", "y"]


    for voyelle in voyelles:
        compteur=0
        for lettre in phrase:
            if lettre == voyelle:
                compteur = compteur + 1
                occurence[lettre]= compteur
    return occurence

phrase = input("saisie de la phrase : ")
nb_voyelles = comptage_voyelles(phrase)
occurences=occurence_voyelle(phrase)
print(nb_voyelles)
print(occurences)